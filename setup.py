from setuptools import setup


def readme():
    with open("README.rst") as f:
        return f.read()


setup(
    name="epitools",
    version="0.1",
    description="Tools for epidemiology and biostatistics",
    long_description=readme(),
    url="https://gitlab.com/alping/epitools",
    author="Peter Alping",
    author_email="peter@alping.se",
    license="MIT",
    packages=["epitools"],
    install_requires=["pyyaml", "numpy", "pandas"],
    zip_safe=False,
)
