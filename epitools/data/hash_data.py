import hashlib
import yaml
from pathlib import Path


def sha256(filename):
    h = hashlib.sha256()
    b = bytearray(128 * 1024)
    mv = memoryview(b)

    with open(filename, "rb", buffering=0) as f:
        for n in iter(lambda: f.readinto(mv), 0):
            h.update(mv[:n])

    return h.hexdigest()


def hash_data(datapath, pattern="*"):
    return {f.name: sha256(f) for f in Path(datapath).glob(pattern)}


def create_hash_data(hashfile, datapath, pattern="*"):
    with open(hashfile, "w") as f:
        yaml.dump(hash_data(datapath), f)


def eq_hash_data(hashfile, datapath, pattern="*"):
    with open(hashfile, 'r') as f:
        old_hash = yaml.safe_load(f)

    new_hash = hash_data(datapath, pattern)

    return old_hash == new_hash
