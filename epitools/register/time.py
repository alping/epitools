import numpy as np
import pandas as pd

# import numba

from typing import List
from numpy import ndarray
from pandas import DataFrame, Series

################################################################################
# Merge Time Intervals
################################################################################
def merge_intervals(intervals: List[List[int]]) -> List[List[int]]:
    intervals = sorted(intervals)

    # Create a stack (list) with the first interval
    stack = [intervals[0]]

    # Start from the next interval and merge if necessary
    for current in intervals[1:]:
        # Get the interval from stack top
        top = stack[-1]
        # If current interval is not overlapping with stack top, push it to the stack
        if top[1] < current[0]:
            stack.append(current)
        # If current ending is larger than top ending, update top ending
        elif top[1] < current[1]:
            stack[-1] = [top[0], current[1]]

    return stack


def merge_intervals_df(df: DataFrame, start: str, stop: str) -> DataFrame:
    return DataFrame(
        merge_intervals(df[[start, stop]].values.tolist()),
        columns=[start, stop],
        dtype="datetime64[ns]",
    )


def merge_intervals_group(
    df: DataFrame, group: str, start: str, stop: str
) -> DataFrame:
    return (
        df.assign(start_stop=lambda x: x[[start, stop]].to_numpy().tolist())
        .groupby(group)["start_stop"]
        .agg(lambda x: x.tolist())
        .apply(merge_intervals)
        .explode()
        .pipe(
            lambda x: pd.DataFrame(
                x.tolist(),
                columns=[start, stop],
                index=x.index,
                dtype="datetime64[ns]",
            )
        )
    )


################################################################################
# Split in Time Intervals
################################################################################
def time_in_interval(
    start: Series, stop: Series, interval_min: float, interval_max: float
) -> Series:
    return (stop.clip(upper=interval_max) - start.clip(lower=interval_min)).clip(0)


def split_time(start: Series, stop: Series, bins: List[float]) -> DataFrame:
    return pd.concat(
        [
            time_in_interval(start, stop, l, u).rename(u if abs(u) > abs(l) else l)
            for l, u in zip(bins[:-1], bins[1:])
        ],
        axis="columns",
    )


def month_categories(values: ndarray) -> ndarray:
    return (np.ceil(np.abs(values)) * np.sign(values)).astype(int)
