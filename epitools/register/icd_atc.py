import re
from numpy import base_repr
from typing import List

################################################################################
# Create regexs to find diagnoses and drugs
################################################################################
range_rgx = re.compile(r"(\d+)-(\d+)")


def regex_range(start: str, stop: str) -> str:
    """Return all numbers between start and stop as a regex, separated by |"""
    assert len(start) == len(stop), "start and stop must have same length"

    return "|".join(
        [str(i).zfill(len(start)) for i in range(int(start), int(stop) + 1)]
    )


def replace_ranges(string: str) -> str:
    """Replaces any (\\d+)-(\\d+) ranges with a pipe separated regex"""
    return range_rgx.sub(lambda x: regex_range(x.group(1), x.group(2)), string)


def build_regex(definition: List[str]) -> str:
    """Build regex from definition"""
    codes = [replace_ranges("".join(d.split())) for d in definition]
    regex = "|".join(codes).replace("(", "(?:")
    return f"(?:{regex})"


def code_group(definition: List[str]) -> str:
    """Build a code capture group from definition"""
    return f"({build_regex(definition)}[A-Z0-9]*)"


def first_code(definition: List[str]) -> str:
    """First code in a string"""
    return rf"^{code_group(definition)}\b"


def first_after_ms(definition: List[str]) -> str:
    """First code after any G35 or G359 codes in a string"""
    return rf"^(?:G359?\s)*{code_group(definition)}\b"


def any_code(definition: List[str]) -> str:
    """Any code in a string"""
    return rf"\b{code_group(definition)}\b"


def any_after_ms(definition: List[str]) -> str:
    """First code or any code after G35 or G359 codes in a string"""
    return rf"^(?:{code_group(definition)}\b|(?:G359?\s)+(?:[A-Z0-9]+\s)*{code_group(definition)}\b)"


################################################################################
# Validate Definitions
################################################################################
inr = r"(\d{1,3}|\d{1,3}-\d{1,3}|\d{3}[A-Z])"
icd = rf"^[A-Z](\d{{1,3}}|\({inr}(\|{inr})*\))?$"
icd_re = re.compile(icd)

anr = r"(\d{1,2}|\d{1,2}-\d{1,2})"
atc = rf"^[A-Z](\d{{1,2}}|\d{{2}}([A-Z]{{1,2}}|[A-Z]{{2}}(\d{{1,2}}|\({inr}(\|{inr})*\))?))?$"
atc_re = re.compile(atc)


def validate_def(definition, type="any"):
    assert type in ["icd", "atc", "any"], "type needs to be 'icd', 'atc', or 'any'"
    if type == "icd":
        return bool(icd_re.match(definition))
    if type == "atc":
        return bool(atc_re.match(definition))
    if type == "any":
        return bool(icd_re.match(definition)) or bool(atc_re.match(definition))


################################################################################
# Other
################################################################################
def icd_range(start, stop):
    return [
        base_repr(i, 36)
        for i in range(int(start, 36), int(stop, 36) + 1)
        if base_repr(i, 36)[1:].isnumeric()
    ]
