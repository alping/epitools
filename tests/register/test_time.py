import numpy as np
import pandas as pd
from epitools.register.time import merge_intervals, merge_intervals_df

# Should test property of no overlap
def test_merge_intervals():
    assert merge_intervals([
        [7, 8], [1, 5], [2, 3], [4, 6], [8, 10], [12, 15]]) == [
        [1, 6], [7, 10], [12, 15]]

# Should test property of no overlap
def test_merge_intervals_df():

    # Setup
    start_date = pd.to_datetime('2005-01-01')

    in_intervals = [[7, 8], [1, 5], [2, 3], [4, 6], [8, 10], [12, 15]]
    in_data = pd.DataFrame(
        dict(
            start=[start_date + pd.DateOffset(i[0]) for i in in_intervals],
            end=[start_date + pd.DateOffset(i[1]) for i in in_intervals]
        )
    )

    expected_intervals = [[1, 6], [7, 10], [12, 15]]
    expected_data = pd.DataFrame(
        dict(
            start=[start_date + pd.DateOffset(i[0]) for i in expected_intervals],
            end=[start_date + pd.DateOffset(i[1]) for i in expected_intervals]
        )
    )

    # Exercise
    result_data = merge_intervals_df(in_data, "start", "end")

    # Verify
    assert result_data.equals(expected_data)

    # Clean up - none
