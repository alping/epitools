import re
from epitools.register.icd_atc import (
    regex_range,
    replace_ranges,
    build_regex,
    first_code,
    any_code,
    first_after_ms,
    validate_def,
    icd_range,
)


def test_regex_range():
    assert regex_range("095", "105") == "095|096|097|098|099|100|101|102|103|104|105"


def test_replace_ranges():
    assert replace_ranges("(1-5)(10-15)") == "(1|2|3|4|5)(10|11|12|13|14|15)"


def test_build_regex():
    assert (
        build_regex(["A (335-343)", "B (01-03)"])
        == "(?:A(?:335|336|337|338|339|340|341|342|343)|B(?:01|02|03))"
    )


def test_first_code():
    assert re.search(first_code(["A33"]), "A330 B67") and not re.search(
        first_code(["A33"]), "B67 A330"
    )


def test_any_code():
    assert (
        re.search(any_code(["A33"]), "A330 B67")
        and re.search(any_code(["A33"]), "B67 A330")
        and not re.search(any_code(["A33"]), "B67 C55")
    )


def test_first_after_ms():
    assert (
        re.search(first_after_ms(["A33"]), "G359 A330 B67")
        and re.search(first_after_ms(["A33"]), "A330 B12")
        and not re.search(first_after_ms(["A33"]), "G359 B67 C55")
    )


def test_validate_def():
    assert (
        validate_def("A(44)", "icd")
        and validate_def("C33BA44", "atc")
        and (validate_def("A(44)") and validate_def("C33BA44"))
    )


def test_icd_range():
    assert icd_range("A95", "B05") == [
        "A95",
        "A96",
        "A97",
        "A98",
        "A99",
        "B00",
        "B01",
        "B02",
        "B03",
        "B04",
        "B05",
    ]
